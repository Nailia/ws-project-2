﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for NewService.xaml
    /// </summary>
    public partial class NewService : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();

        public NewService()
        {
            InitializeComponent();
            startclock();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }
        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            lblCurrentTime.Content = DateTime.Now.ToString();
        }

        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }

        private void miNewService_Click(object sender, RoutedEventArgs e)
        {
            NewService nc = new NewService();
            nc.Show();
            this.Hide();
        }

        private void miServiceList_Click(object sender, RoutedEventArgs e)
        {
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }

        private void btnAddService_Click(object sender, RoutedEventArgs e)
        {
            double price;
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            //checking if required fields are empty or not
            if (txtServiceName.Text == "" || txtPrice.Text == "")
            {
                MessageBoxResult result = MessageBox.Show("These fields are required");
            }           
            else if (double.TryParse(txtPrice.Text, out price) != true)
            {
                MessageBox.Show("Price must be a number");
            }
            else
            {
                string serviceName = txtServiceName.Text;
                double Price = double.Parse(txtPrice.Text);

                // Check this service for his existance in the Services List
                string j = objWebservice.checkService(serviceName, Price);

                if (j == "")
                {
                    // Insert this new service
                    int i = objWebservice.AddNewService(serviceName, Price);

                    if (i != 0)
                    {
                        if (MessageBox.Show("Service was saved successfully.\n" +
                            "Do you want to add another Service?",
                            "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        {
                            //this.Show();
                            txtServiceName.Text = "";
                            txtPrice.Text = "";
             
                        }
                        else
                        {
                            this.Hide();
                            new ServicesList().Show();
                        }
                    }

                }
                else //Service already in DB
                {
                    MessageBox.Show("This service is already in the list");
                }
            }

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new MainWindow().Show();
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            txtServiceName.Text = "";
            txtPrice.Text = "";
        }
    }
}
