﻿using HiDentClient.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for chart.xaml
    /// </summary>
    public partial class chart : Window
    {
        private List<object> MyCollection;
        public chart()
        {
            InitializeComponent();
            startclock();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }
        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            lblCurrentTime.Content = DateTime.Now.ToString();
        }

        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }

        private void miNewService_Click(object sender, RoutedEventArgs e)
        {
            NewService nc = new NewService();
            nc.Show();
            this.Hide();
        }

        private void miServiceList_Click(object sender, RoutedEventArgs e)
        {
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }

        private void miNewDoctor_Click(object sender, RoutedEventArgs e)
        {
            NewDoctor nd = new NewDoctor();
            nd.Show();
            this.Hide();
        }

        private void miDoctorsList_Click(object sender, RoutedEventArgs e)
        {
            DoctorsList dl = new DoctorsList();
            dl.Show();
            this.Hide();
        }

        private void newReceipt_Click(object sender, RoutedEventArgs e)
        {
            CreateReceipt cr = new CreateReceipt();
            cr.Show();
            this.Hide();
        }

        private void findReceipt_Click(object sender, RoutedEventArgs e)
        {
            ReceiptsList rl = new ReceiptsList();
            rl.Show();
            this.Hide();
        }

        private void printReport_Click(object sender, RoutedEventArgs e)
        {
            PrintReport pr = new PrintReport();
            pr.Show();
            this.Hide();
        }

        private void miChart_Click(object sender, RoutedEventArgs e)
        {
            chart ch = new chart();
            ch.Show();
            this.Hide();
        }

        private void myChart_Loaded(object sender, RoutedEventArgs e)
        {
            using (var db = new HiDentEntities2())
            {
                MyCollection = new List<object>();
                /*
                select YEAR(r.Date), sum(s.Price) as "h" from Services s, Receipt r where r.ServiceID = s.Id group by YEAR(r.Date);
                 */
                var query = from r in db.Receipt
                            from s in db.Services
                            where r.ServiceID == s.Id
                            group s by r.Date
                            into g
                            select new { Service = g.Key, Income = g.Sum(s => s.Price) };
                foreach (var item in query)
                {
                    MyCollection.Add(item);
                }
                ((ColumnSeries) myChart.Series[0]).ItemsSource = MyCollection;
            }
        }
        


    }
}
