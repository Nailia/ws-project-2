﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for ModifyDoctor.xaml
    /// </summary>
    public partial class ModifyDoctor : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();
        int idDoc;

        public ModifyDoctor(int Id, string firstName, string lastName, string medNum)
        {
            InitializeComponent();
            idDoc = Id;
            txtFirstName.Text = firstName;
            txtLastName.Text = lastName;
            txtMedNum.Text = medNum;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            startclock();
        }
        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            lblCurrentTime.Content = DateTime.Now.ToString();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new MainWindow().Show();
        }

        private void btnUpdateDoctor_Click(object sender, RoutedEventArgs e)
        {
            string firstName = txtFirstName.Text;
            string lastName = txtLastName.Text;
            string medNum = txtMedNum.Text;
            int i = objWebservice.editDoctor(idDoc, firstName, lastName, medNum);
            if (i >= 1)
            {
                MessageBox.Show("Doctor updated successfully");
            }
            else
            {
                MessageBox.Show("Error update");
            }
            DoctorsList nc = new DoctorsList();
            nc.Show();
            this.Hide();
        }

        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }

        private void miNewService_Click(object sender, RoutedEventArgs e)
        {
            NewService nc = new NewService();
            nc.Show();
            this.Hide();
        }

        private void miServiceList_Click(object sender, RoutedEventArgs e)
        {
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }
    }
}
