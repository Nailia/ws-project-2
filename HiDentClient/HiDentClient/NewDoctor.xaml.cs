﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for NewDoctor.xaml
    /// </summary>
    public partial class NewDoctor : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();

        //string firstname, lastname, ;

        public NewDoctor()
        {
            InitializeComponent();
            startclock();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }
        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            lblCurrentTime.Content = DateTime.Now.ToString();
        }

        private void btnAddDoctor_Click(object sender, RoutedEventArgs e)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            //checking if required fields are empty or not
            if (txtLastName.Text == "" || txtFirstName.Text == "")
            {
                MessageBoxResult result = MessageBox.Show("First name and Last name are required");
            }
            else
            {
                string firstName = txtFirstName.Text;
                string lastName = txtLastName.Text;
                string medNum = txtMedNumber.Text;     

                // Check this doctor for his existance
                string j = objWebservice.checkDoctor(firstName, lastName);

                if (j == "")
                {
                    // Insert this new doctor
                    int i = objWebservice.AddNewDoctor(firstName, lastName, medNum);

                    if (i != 0)
                    {
                        if (MessageBox.Show("Doctor's information was saved successfully.\n" +
                            "Do you want to add another Doctor?",
                            "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        {
                            //this.Show();
                            txtFirstName.Text = "";
                            txtLastName.Text = "";
                            txtMedNumber.Text = "";
           
                        }
                        else
                        {
                            this.Hide();
                            new MainWindow().Show();
                        }
                    }

                }
                else //Doctor is already in DB
                {
                    MessageBox.Show("This doctor is already registered");
                }
            }

        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtMedNumber.Text = "";
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new MainWindow().Show();
        }
        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miNewService_Click(object sender, RoutedEventArgs e)
        {
            NewService nc = new NewService();
            nc.Show();
            this.Hide();
        }

        private void miServiceList_Click(object sender, RoutedEventArgs e)
        {
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); 
        }

    }
}
