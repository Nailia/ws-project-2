﻿using HiDentClient.localhost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for CreateReceipt.xaml
    /// </summary
    /// 
    

    public partial class CreateReceipt : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();
        public CreateReceipt()
        {
            InitializeComponent();
            startclock();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;

            //Doctor comboBox
            List<ComboData> doctorData = new List<ComboData>(objWebservice.getDoctorsList());
            cbDoctorNumber.ItemsSource = doctorData;
            cbDoctorNumber.DisplayMemberPath = "Value";
            cbDoctorNumber.SelectedValuePath = "Id";            
            cbDoctorNumber.SelectedIndex = 0;
            
            //Service comboBox
            List<ComboData> serviceData = new List<ComboData>(objWebservice.getAllServices());
            cbServices.ItemsSource = serviceData;
            cbServices.DisplayMemberPath = "Value";
            cbServices.SelectedValuePath = "Id";
            cbServices.SelectedIndex = 0;
            
        }

        private void btnCreateReceipt_Click(object sender, RoutedEventArgs e)
        {
            string firstName = txtCustFirstName.Text;
            string lastName = txtCustLastName.Text;
            if (cbDoctorNumber.SelectedIndex == 0)
            {
                MessageBox.Show("Please select Doctor ID Number");
            }
            else if (txtCustFirstName.Text == "" || txtCustLastName.Text == "")
            {
                MessageBox.Show("Customer's Firs and Last Names are required");
            }
            //check for customer name
            else if (objWebservice.checkCustomer(firstName, lastName) == "")
            {
                MessageBox.Show("You don't have a customer under name of " + firstName + " " + lastName);
            }
            else if (cbServices.SelectedIndex == 0)
            {
                MessageBox.Show("Please choose a service");
            }
            else if (dpReceiptDate.SelectedDate == null)
            {
                MessageBox.Show("Please choose a date");
            }
            else
            {
                int doctorID = int.Parse(cbDoctorNumber.SelectedValue.ToString());
                int serviceID = int.Parse(cbServices.SelectedValue.ToString());
                int custID = int.Parse(objWebservice.checkCustomer(firstName, lastName));
                DateTime date = DateTime.Parse(dpReceiptDate.SelectedDate.ToString());
                if (objWebservice.saveReceipt(doctorID, custID, serviceID, date) > 0)
                {
                    MessageBox.Show("New receipt was created successfully");
                    new ReceiptsList().Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("New receipt wasn't created successfully. There are some issues");
                }
            }              

        }  


        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int ServiceID = int.Parse(cbServices.SelectedValue.ToString());
            txtAmount.Text = objWebservice.getServicePriceById(ServiceID);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new MainWindow().Show();
        }

        private void cbDoctorNumber_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            cbDoctorNumber.SelectedIndex = 0;
            txtCustFirstName.Text = "";
            txtCustLastName.Text = "";
            cbServices.SelectedIndex = 0;
            txtAmount.Text = "";
            dpReceiptDate.Text = null;
        }
        //Menu Bar
        #region        
        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            lblCurrentTime.Content = DateTime.Now.ToString();
        }

        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }

        private void miNewService_Click(object sender, RoutedEventArgs e)
        {
            NewService nc = new NewService();
            nc.Show();
            this.Hide();
        }

        private void miServiceList_Click(object sender, RoutedEventArgs e)
        {
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }
        

        private void findReceipt_Click(object sender, RoutedEventArgs e)
        {
            ReceiptsList rl = new ReceiptsList();
            rl.Show();
            this.Hide();
        }

        private void printReport_Click(object sender, RoutedEventArgs e)
        {
            PrintReport pr = new PrintReport();
            pr.Show();
            this.Hide();
        }

        private void newReceipt_Click(object sender, RoutedEventArgs e)
        {
            new CreateReceipt().Show();
            this.Hide();
        }
        #endregion

    }
}
