﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for DoctorsList.xaml
    /// </summary>
    public partial class DoctorsList : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();
        int count;

        public DoctorsList()
        {
            InitializeComponent();
            dgDoctors.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.displayDoctorsList() });
            startclock();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            count = dgDoctors.Items.Count;
            lblCount.Content = "Total doctors: " + (count-1);
        }
        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            lblCurrentTime.Content = DateTime.Now.ToString();
        }

        private void btnModify_Click(object sender, RoutedEventArgs e)
        {
            DataRowView rowview = dgDoctors.SelectedItem as DataRowView;
            int id = int.Parse(rowview.Row[0].ToString());
            string firstName = rowview.Row[1].ToString();
            string lastName = rowview.Row[2].ToString();
            string medNum = rowview.Row[3].ToString();
            ModifyDoctor nc = new ModifyDoctor(id, firstName, lastName, medNum);
            nc.Show();
            this.Hide();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            DataRowView rowview = dgDoctors.SelectedItem as DataRowView;
            int id = int.Parse(rowview.Row[0].ToString());

            if (MessageBox.Show("Are you Sure you want to Delete " + rowview.Row[1].ToString() + " " + rowview.Row[2].ToString() + "?",
                                         "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                int j = objWebservice.deleteDoctor(id);
                if (j > 0)
                {
                    MessageBox.Show("Record Deleted Successfully!");
                    dgDoctors.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = objWebservice.displayDoctorsList() });
                }
                this.Show();
            }
            count = dgDoctors.Items.Count;
            lblCount.Content = "Total doctors: " + (count - 1);
        }

        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }

        private void miNewService_Click(object sender, RoutedEventArgs e)
        {
            NewService nc = new NewService();
            nc.Show();
            this.Hide();
        }

        private void miServiceList_Click(object sender, RoutedEventArgs e)
        {
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }

    }
}
