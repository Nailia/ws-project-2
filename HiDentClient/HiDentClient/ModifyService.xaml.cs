﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for ModifyService.xaml
    /// </summary>
    public partial class ModifyService : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();
        int idService;

        public ModifyService(int Id, string serviceName, double Price)
        {
            InitializeComponent();
            idService = Id;
            txtServiceName.Text = serviceName;
            txtPrice.Text = Price.ToString();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            startclock();
        }
        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            lblCurrentTime.Content = DateTime.Now.ToString();
        }

        private void btnUpdateClient_Click(object sender, RoutedEventArgs e)
        {
            string serviceName = txtServiceName.Text;
            int Price = int.Parse(txtPrice.Text);

            int i = objWebservice.editService(idService, serviceName, Price);
            if (i >= 1)
            {
                MessageBox.Show("Service updated successfully");
            }
            else
            {
                MessageBox.Show("Error update");
            }
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }

        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }

        private void miNewService_Click(object sender, RoutedEventArgs e)
        {
            NewService nc = new NewService();
            nc.Show();
            this.Hide();
        }

        private void miServiceList_Click(object sender, RoutedEventArgs e)
        {
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new ServicesList().Show();
        }
    }
}
