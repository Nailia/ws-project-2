﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HiDentClient
{
    /// <summary>
    /// Interaction logic for ModifyCustomer.xaml
    /// </summary>
    public partial class ModifyCustomer : Window
    {
        localhost.WebService objWebservice = new localhost.WebService();
        int idCust;
        public ModifyCustomer(int Id, string firstName, string lastName, string address, string city, string postalCode, string country, string phone, string email)
        {
            InitializeComponent();
            idCust = Id;
            txtFirstName.Text = firstName;
            txtLastName.Text = lastName;
            txtAddress.Text = address;
            txtCity.Text = city;
            txtPostalCode.Text = postalCode;
            txtCountry.Text = country;
            txtPhone.Text = phone;
            txtEmail.Text = email;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            startclock();
        }
        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();
        }

        private void tickevent(object sender, EventArgs e)
        {
            lblCurrentTime.Content = DateTime.Now.ToString();
        }

        private void btnUpdateClient_Click(object sender, RoutedEventArgs e)
        {
            string firstName = txtFirstName.Text;
            string lastName = txtLastName.Text;
            string address = txtAddress.Text;
            string city = txtCity.Text;
            string postalCode = txtPostalCode.Text;
            string country = txtCountry.Text;
            string phone = txtPhone.Text;
            string email = txtEmail.Text;
            int i = objWebservice.editCustomer(idCust, firstName, lastName, address, city, postalCode, country, phone, email);
            if (i >= 1)
            {
                MessageBox.Show("Customer updated successfully");
            } else
            {
                MessageBox.Show("Error update");
            }
            FindCustomer nc = new FindCustomer();
            nc.Show();
            this.Hide();
        }
        private void miNewClient_Click(object sender, RoutedEventArgs e)
        {
            NewCustomer nc = new NewCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        private void miFind_Client_Click(object sender, RoutedEventArgs e)
        {
            FindCustomer nc = new FindCustomer();
            nc.Show();  // or nc.Showdialog();
            this.Hide();
        }

        //private void miDeleteClient_Click(object sender, RoutedEventArgs e)
        //{
        //    DeleteCustomer nc = new DeleteCustomer();
        //    nc.Show();  // or nc.Showdialog();
        //    this.Hide();
        //}

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); //or Shutdown the application:   Application.Current.Shutdown();
        }

        private void miNewService_Click(object sender, RoutedEventArgs e)
        {
            NewService nc = new NewService();
            nc.Show();
            this.Hide();
        }

        private void miServiceList_Click(object sender, RoutedEventArgs e)
        {
            ServicesList nc = new ServicesList();
            nc.Show();
            this.Hide();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            new MainWindow().Show();
        }
    }
}
